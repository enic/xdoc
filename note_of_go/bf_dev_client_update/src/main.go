package main

import (
	_ "fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
)



func main() {
    logrus.SetLevel(logrus.TraceLevel)
    logrus.SetReportCaller(true)
    http_client := &http.Client{}
    http_res, err := http_client.Get("http://10.253.170.156:8000/Trunk/c1")
    if err != nil {
        logrus.Error(err)
    }
    defer http_res.Body.Close()
    body_data, err := ioutil.ReadAll(http_res.Body)
    if err != nil {
        logrus.Error(err)
    }
    doc := string(body_data)
    logrus.Debug(doc)

    dom, err := goquery.NewDocumentFromReader(strings.NewReader(doc)) 
    logrus.Debug(dom)
    dom.Find("a").Each(func(i int, selection *goquery.Selection){
        logrus.Debug("1---", i, selection.Text())
        selection.Find("a").Each(func(i int, selection *goquery.Selection){
            logrus.Debug("2---", i, selection.Text())
        })
    })
}