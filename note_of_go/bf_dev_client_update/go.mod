module bf_dev_client_update

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.8.0 // indirect
	github.com/alecthomas/log4go v0.0.0-20180109082532-d146e6b86faa // indirect
	github.com/gohutool/log4go v1.0.2 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
)
