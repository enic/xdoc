## ustl迭代器
**没有标准stl迭代器那么完善**
#### 迭代器类型
* reverse_iterator
* insert_iterator
* back_insert_iterator
* index_iterate
*源码中只定义这几个迭代器类型*

#### iterator_traits
这个是标准实现
```cpp
tempalte <typename Iterator>
struct iterator_traits
{
    typedef typename Iterator::value_type      value_type;
    typedef typename Iterator::difference_type difference_type;
    typedef typename Iterator::pointer         pointer;
    typedef typename Iterator::reference       reference;
}
```
跟着四个（偏）特化：指针 常量指针 void*类型 const void*类型
这里的void相关实现value为uint8无法理解，而且应该还有bool类型也需要特化
按目前的理解traits传入的实际是迭代器的类型，当迭代器为void*类型的时候，说明无意义，所以就用一个占用位置最少的类型替代
针对bool的特化可能需要将整个vector特化掉，这样才能达到最好的存储效率 
```cpp
template <typename T>
struct iterator_traits<T*>
{
    typedef T         value_type;
    typedef ptrdiff_t difference_type;
    typedef const T*  const_pointer;
    typedef T*        pointer;
    typedef const T&  const_reference;
    typedef T&        reference;
};

template <typename T>
struct iterator_traits<const T*>
{
  typedef T           value_type;
  typedef ptrdiff_t   difference_type;
  typedef const T*    const_pointer;
  typedef const T*    pointer;
  typedef const T&    const_reference;
  typedef const T&    reference;
};

template <>
struct iterator_traits<void*>
{
    typedef uint8_t          value_type;
    typedef ptrdiff_t        difference_type;
    typedef const void*      const_pointer;
    typedef void*            pointer;
    typedef const value_type const_reference;
    typedef value_type       reference;
}

template <>
struct iterator_traits<const void*>
{
    typedef uint8_t              value_type;
    typedef ptrdiff_t            difference_type;
    typedef const void*          const_pointer;
    typedef const void*          pointer;
    typedef const value_type&    const_reference;
    typedef const value_type&    reference;
}

```