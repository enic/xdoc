#内存管理：
####class cmemlink： 有点像MemBlockView，持有内存指针，但是并不管理内存的声明周期
有stl like的traits，但是没有魔板参数，value是char类型
特色接口：
  stream_align_of(T&)
  write(osteam&)
  text_write(ostringstream, readable_size())
  readable_size()
  stream_size()
  write_file(filename, mode)
  max_size: 按说这个和下面的readable_size是steam概念下才需要的接口
  readable_size:
  link：这一组接口没看出来设计意图，他也没有管理内存
  relink：
  unlink：

####class memlink : public cmemlink: 同样没有管理内存的生命周期，除了扩展cmemlink支持更多的操作接口，暂时看不出存在价值
多了stl like的 inset erase rotate类似的接口

####class memblock : public memlink：这一级可以管理内存声明周期了，同时也多了更多stl like接口，例如 assign reisze reserve
值得留意的是：link细节接口都是不关心内存声明周期的。
整个类读下来，只有析构或者缩容的时候会调整内存
特色接口：
  shrink_to_fit：根据实际数据大小对内存缩容，这个是stl里边完全没有的特性
  is_linked: 不太理解设计意图，但实际作用是检查内部有没有动态分配内存。
  manage: 这个会判断当前自己是否有管理内存，如果有会断言

#### 关键内存分配函数和操作符hook
C/C++接口内存分配的常规套路需要处理这么几个玩意：
void* malloc(size_t n)
void* realloc(void* p, size_t n)
void  free(void* p) 
void* operator new (size_t n)
void* operator new[] (size_t n)
void  operator delete (void* p)
void  operator delete[] (void* p)
void* operator new (size_t, void* p)
void* operator new[] (size_t, void* p)
void  operator delete (void*, void*)
void  operator delete[] (void*, void*)
**如果一个带额外参数的operator new没有“带相同参数”的对应版operator delete，那么就当new的内存分配动作取消并恢复原样时就没有任何operator delete被调用，于是产生内存泄露。**


ustl默认没有统一接管内存的，但是开好了各种重载函数，如果需要定制，直接使用宏开启然后把自己的实现填进去即可。








