# Maven功能
* 构建
* 文档生成
* 报告
* 依赖
* SCMS(software configuration Management)
* 发布
* 分发
* 邮件列表

## 约定配置
maven提倡使用共同标准的目录结构，maven使用约定优与配置的原则，约定目录结构如下：
base_dir: 存放pom.xml的子目录
base_dir/src/main/java: 项目的java源代码
base_dir/src/main/resource: 项目的资源比如property文件，springmvc.xml文件
base_dir/src/test/java: 测试文件，比如junit代码
base_dir/src/test/resource: 测试资源
base_dir/src/main/webapp/WEB-INF: web应用文件目录，web项目信息，比如web.xml、本地图片、jsp
视图页面
base_dir/target: 打包输出目录
base_dir/target/classes：编译输出目录
base_dir/target/test-classes: 测试输出目录
Test.java：maven只会运行负荷该命名规则的测试类
~/.m2/repository： maven默认的本地仓库目录位置

a8d711c566a344b09a6ede9bcc7896fe
## 常用命令
* mvn clean
* mvn clean compile
* mvn clean package
* mvn clean install
* mvn clean deploy

## 关键标签
* project: 跟节点
  * parent: 类似继承，复用依赖，减少冗余配置
  * modelVersion: 当前xml使用的规范版本
  * groupId: 定义项目属于哪个项目组。可以随意命名
  * artifactId: 定义当前项目在项目组中的唯一ID
  * version: 当前项目的版本
  * name: 当前项目的名字
* dependencies: 依赖配置
  * dependency: 某个依赖的详细信息
    * groupId: 依赖的项目组
    * artifactId: 依赖项目的组内唯一标识
    * version: 依赖的版本
    * type: 类型，默认为jar
    * scope: 依赖的范围
    * optional: 是否可选依赖
    * exclusions: 排除传递性依赖(循环引用？)

## 依赖下载的位置
默认在 ~/.m2/repository

go get -u github.com/cheat/cheat/cmd/cheat