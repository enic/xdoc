#pragma once

struct MessageAny
{
    FName TypeName,
    const void* Value;

    template <typename T>
    static MessageAny MakeMsg(const T& t)
    {
        return MessageAny{TClass2Name<typename TRemoveReference<typename TRemoveCV<T>::Type>::Type>::GetName(), &t};
    }

    template <typename T>
	static MessageAny MakeMsg(const T* t)
	{
		return MessageAny{TClass2Name<typename TRemoveReference<typename TRemoveCV<T>::Type>::Type>::GetName(), t};
	}
};

struct FNZMessage
{
    UNZMessageManager* Mgr;
    FString MessageId;
    uint64 Sequence = GetSequenceID();
    TArray<MessageAny> Params;

private:
    template <>
}