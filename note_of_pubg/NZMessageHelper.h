#pragma once

class FNZMessageHelper
{
public:
    static void ShutDown(UObject* Obj);

    /**
     * @brief Get the Data Server object
     * 
     * @return DataServerType* 
     */
    template <typename DataServerType>
    static DataServerType* GetDataServer(UObject* Obj)
    {
        check(Obj);

        return DataServerType::Get<DataServerType>(Obj);
    }

    static void UnListenNZMessage(UObject* Obj, FString MessageId, uint64 id);

    template <typename T, typename F>
    static uint64 ListenNZMessage(T* Obj, consg FString& MessageId, F&& f, int32 Times = -1);

    template <typename T, typename F>
    static uint64 ListenNZMessageAny(T* Obj, consg FString& MessageId, F&& f, int32 Times = -1);
};