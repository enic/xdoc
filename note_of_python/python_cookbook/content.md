# [1. 目录](https://python3-cookbook.readthedocs.io/zh_CN/)

* [第一章：数据结构与算法](ch1/1.md)
  * [1.1 将序列分解为单独的变量](ch1/1.1.md)
  * [1.2 解包可迭代对象复制给多个变量](ch1/1.2.md) 
