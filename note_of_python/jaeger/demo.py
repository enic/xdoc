# -*- coding: utf-8 -*-

# sudo pip3 install --upgrade jaeger-client-python -i https://pip.nie.netease.com/simple
# sudo pip3 install opentracing

import logging
import opentracing
import time
from jaeger_client.config import Config
from jaeger_client.constants import REPORTER_TYPE_SPAN_BATCH_LOG

logger = logging.getLogger("main_test")
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

def init_tracer():
    config = Config(
        config={
            'sampler': {
                'type': 'const',
                'param': 1,
            },
            # 指定数据提交类型和 logger
            'type': REPORTER_TYPE_SPAN_BATCH_LOG,
            'logger': logger,
            'tags': {'project': 'sigma'}
        },
        service_name='test-demo-h42-2',
        validate=True,
    )
    tracer = config.initialize_tracer()
    return tracer

if __name__ == '__main__':
    tracer = init_tracer()
    with tracer.start_span('s1') as s1:
        s1.log_kv({"key":"value"})
        with tracer.start_span('s2',child_of=s1) as s2:
            s2.log_kv({"key2":"value2"})
    time.sleep(1)
    tracer.close()
    time.sleep(1)


