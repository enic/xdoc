#%%
import functools
# items = [{"uid":2,"name":2},{"uid":3,"name":3},{"uid":1,"name":1}]
# # 指定字段升序
# items.sort(key=lambda item: item["uid"], reverse=False)
# print(items)
# # 指定字段降序
# items.sort(key=lambda item: item["uid"], reverse=True)
# print(items)

items = [{"uid":2,"name":"2"},{"uid":3,"name":"3"},{"uid":1,"name":"1"}]
#items = [{"uid":2,"name":"1"},{"uid":3,"name":"3"},{"uid":1,"name":"2"}]
#items = [{"uid":2,"name":1},{"uid":3,"name":3},{"uid":1,"name":2}]
# list不支持自定义比较函数,但是可以通过functools实现: uid升序
#items.sort(cmp=lambda item: item["uid"] < item["uid"])
def cmp2k_uid(left, right):
    return left["uid"] - right["uid"]
def cmp_uid(left, right):
    if left["uid"] > right["uid"]:
        return 1
    elif left["uid"] == right["uid"]:
        return 0
    else:
        return -1
def cmp_name(left, right):
    return left["name"] < right["name"]
items.sort(key=functools.cmp_to_key(cmp2k_uid))
print(items)
items = [{"uid":2,"name":"2"},{"uid":3,"name":"3"},{"uid":1,"name":"1"}]
items.sort(key=functools.cmp_to_key(cmp_uid))
print(items)
# items.sort(key=functools.cmp_to_key(cmp_name))
# print(items)
# items.sort(key=functools.cmp_to_key(cmp_name), reverse=True)
# print(items)
# # name排序
#items.sort(key=functools.cmp_to_key(lambda left, right: left["name"] < right["name"]))
#print(items)
# uid优先和name联合排序
#items.sort(key=functools.cmp_to_key(lambda left, right: left["uid"] < right["uid"] or left["name"] < right["name"]))
#print(items)
##%%

# %%