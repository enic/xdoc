# coding: utf8

UID_ZONE_BIT            = 34    #uid中,zone左移的位数(默认情况下)
UID_REGION_BIT          = 48    #uid中,region左移的位数(默认情况下)
CORPS_UID_BEGIN         = 1000000000 #军团id起始
AI_PLAYER_SEQ_MOD=7  

def is_self_room_ai_player(uid):
    '''
    @brief: 是否自建房ai
    '''
    return uid <= 100

def is_ai_player(uid):
    #自建房AI100以内
    if is_self_room_ai_player(uid):
        return True
    uid = uid & ((1 << UID_ZONE_BIT) - 1)
    return uid % AI_PLAYER_SEQ_MOD == 0

def get_seq(uid):
    uid = uid & ((1 << UID_ZONE_BIT) - 1)
    return uid

def is_bot_uid(uid):
    return uid <= 0xffffffff and not is_self_room_ai_player(uid)

def is_real_player(uid):
    '''
    @brief: 是否真人玩家id
    '''
    return not is_ai_player(uid) and not is_bot_uid(uid)

print(is_ai_player(1424554752760489))
print(is_ai_player(17179869200376)) 
print(is_ai_player(17179869200374)) 
print(is_ai_player(17179869200315))
print(is_ai_player(17179869200316))
print(is_ai_player(17179869200310))
print(is_ai_player(1424554752760489))
print(is_ai_player(17179869200314))
print(is_ai_player(17179869199019))
print(is_ai_player(17179869200313))
print(is_ai_player(17179869202127))
print(is_ai_player(17179869200225)) 
print(is_ai_player(17179869200311)) 
print(is_ai_player(1424554752759219))