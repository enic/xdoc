# -*- coding: utf-8 -*-

import sys
import win32api
import skia
import contextlib
import glfw
from OpenGL import GL
from PySide6 import QtCore, QtWidgets, QtGui


class CustomDrawWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.button = QtWidgets.QPushButton("中文按钮", self)
        self.button.resize(self.button.sizeHint())
        self.button.move(100,100)
        self.button.clicked.connect(self.on_clicked)

    def paintEvent(self, event:QtGui.QPaintEvent) -> None:
        #super().paintEvent(event)
        painter = QtGui.QPainter(self)
        return

    def on_clicked(self):
        sys.exit(0)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    w = CustomDrawWidget()
    w.resize(800, 600)
    w.show()
    sys.exit(app.exec_())