# coding: utf8

import logging
from init_log import setup_logger

setup_logger()
logger = logging.getLogger()

def coro_func(param): 
    print("in corotine: s1", param)
    ret = yield "pass to schedule s1"
    print("in corotine: s2", param, ret)
    ret = yield "pass to schedule s2"
    print("in corotine: s3", param, ret)
    
 
# # print(coro_func)

# # print(coro_func(5))
# # for n in coro_func(5): 
# #     print(type(n))
# #     print(n)

# print(coro_func)
# print(coro_func(5))
# g1 = coro_func(5)
# print(1, g1)
# g2 = coro_func(5)
# print(2, g2)
# g3 = coro_func(5)
# print(3, g3)


# # for ret in g1:
# #     print(ret)


# # if g1:
# #     ret = next(g1)
# #     print("schedule get ret", ret)
# # print("back")
# # if g1:
# #     ret = next(g1)
# #     print("schedule get ret", ret)
# # print("back 2")

# ret = next(g1)
# print("schedule get ret:", ret)
# ret = g1.send("ssssssssssss")
# print("schedule get ret:", ret)

# next(g1, "sssssssssaaaaaaaaaaaaaaaaaaaaaa")
# #g1.send("ssssssssssssaaa")
# print("schedule get ret:", ret)

# g1.close()


class CoroutineMgr:
    INVALID_CO_ID = -1  # 无效协程ID
    def __init__(self):
        ''''''
        self.co_id_seed = 0
        #self.cur_co_id = CoroutineMgr.INVALID_CO_ID
        self.id2task = {}

    def _next_coro_id(self):
        self.co_id_seed += 1
        return self.co_id_seed

    def _on_coroutine_task_finish(self, task):
        ''''''
        co_id = task.co_id
        logger.debug("coroutine task finish: co_id=%s", co_id)
        if co_id in self.id2task:
            del self.id2task[co_id]
        else:
            logger.info("coroutine task is not under manager: co_id=%s", co_id)

    def create(self, co_func):
        ''''''
        task = CoroutineTask(co_func)
        last_co_id = self.co_id_seed
        co_id = self._next_coro_id()
        while(co_id in self.id2task):
            co_id = self._next_coro_id()
        task.co_id = co_id
        task.cb_finish = self._on_coroutine_task_finish
        self.id2task[co_id] = task
        logger.debug("create coroutine: last_co_id=%s,co_id=%s", last_co_id, co_id)
        return task.co_id

    def resume(self, coro_id, *args):
        ''''''
        task: CoroutineTask = self.id2task.get(coro_id)
        if not task:
            logger.info("can't find coroutine task: coro_id=%s", coro_id)
            return
        task.resume(*args)

    def close(coro_id):
        ''''''

        
class CoroutineTask:
    CORO_TASK_IDEL = 0     # 刚刚创建好 
    CORO_TASK_RUNNING = 0  # 运行中
    CORO_TASK_CLOSED = 0   # 已结束
    def __init__(self, func, *args):
        self.co_id = CoroutineMgr.INVALID_CO_ID
        self.func = func
        self.args = args
        self.state = 0
        self.gen = None
        self.cb_finish = None

    def resume(self, *args):
        if not self.gen:
            self._start(*args)
        else:
            self.gen.send(*args)

    def close(self):
        self.gen.close()

    def _start(self, *args):
        self.args = args
        self.gen = self._run()
        next(self.gen)

    def _run(self):
        logging.debug("try to run real coroutine function")
        yield from self.func(*self.args)
        logging.debug("finish to run real coroutine function")
        self._finish()
        yield

    def _finish(self):
        self.cb_finish(self)

        


def call_rpc(c1, c2):
    logger.debug("call_rpc: c1=%s", c1)
    c1_ret = yield
    logger.debug("c1_ret=%s", c1)
    logger.debug("call_rpc: c2=%s", c2)
    c2_ret = yield
    logger.debug("c2_ret=%s", c2_ret)
    xxx_task()

def xxx_task():
    logging.info("xxxxxxxxxxxxxxx")
    yield

coroutine_mgr = CoroutineMgr()
co_id = coroutine_mgr.create(call_rpc)
logger.debug("try start: co_id=%s", co_id)
logger.debug("resume 1")
coroutine_mgr.resume(co_id, "start_p1", "start_p2")
logger.debug("resume 2")
coroutine_mgr.resume(co_id, "response call c1")

coroutine_mgr.resume(co_id, "response call c2")
    
    
    