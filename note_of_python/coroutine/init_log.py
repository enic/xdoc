import logging
import sys

logger = logging.getLogger()
    
def setup_logger():
    # 设置日志配置
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s]: %(message)s')
    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(formatter)
    sh.setLevel(logging.DEBUG)
    logger.addHandler(sh)
    file_sh = logging.FileHandler("coroutine.log")
    file_sh.setFormatter(formatter)
    file_sh.setLevel(logging.DEBUG)
    logger.addHandler(file_sh)