# coding: utf-8
import json

uids = [
    1579173580798175,	
    1579173580146930,	
    1579173580680070,	
    1579173578719665,	
    1579173580819060,	
    1579173580800039,	
    1579173577281624,	
    1579173577351343,	
    1579173577866790,	
    1579173576519967,	
    1579173576370605,	
    1579173578031472,	
]

path_tmp = "data/%s_battledata_personal.json"

# player_info
player_key_map = {
    "击杀" : "kill_num",
    "死亡" : "dead_num",
    "助攻" : "assist_num",
    "爆头击杀" : "head_kill",
    #"回合总数" : "round_cnt",
    "总伤害" : "total_damage",
}

idx = 0
for uid in uids:
    file = open(path_tmp % uid, 'r', encoding="utf-8")
    data = file.read()
    idx += 1
    bdata = json.loads(data[1:-1])
    print("-------------------------: uid=%s" % uid)
    for battledata in bdata.get("battledata_map", {}).values():
        tmp = {}
        tmp["uid"] = bdata["_id"]
        tmp["battle_id"] = battledata["battle_id"]
        tmp["game_play_mode"] = battledata["game_play_mode"]
        round_cnt_attack = battledata["attack"].get("round_count", 0)
        round_cnt_defence = battledata["defence"].get("round_count", 0)
        tmp["round_cnt"] = round_cnt_attack + round_cnt_defence
        player_info = battledata["player_info"]
        for k, dt_key in player_key_map.items():
            tmp[k] = player_info[dt_key]
        weapon_data = battledata["weapons"]
        tmp["weapon_cnt"] = len(weapon_data)
        print(tmp)
        for weapon in weapon_data:
            print("",  {
                "weapon_id" : weapon["weapon_id"],
                "head" : weapon["head"],
                "body" : weapon["body"],
                "leg" : weapon["leg"],
            })