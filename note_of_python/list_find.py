import sys, time, random


def list_find_idx(lst:list, val):
    idx = -1
    try:
        idx = lst.index(val)
    except Exception:
        idx = -1
    return idx

if __name__ == '__main__':
    data_len = 1000
    lst = []
    for n in range(0, data_len):
        lst.append(n)

    test_cnt = 10000
    start_time = time.time()
    find_cnt = 0
    for n in range(0, test_cnt):
        tar_val = random.randint(-data_len, data_len) #test_cnt
        if list_find_idx(lst, tar_val) >= 0:
            find_cnt += 1
    print("list_find_idx cost: ", time.time() - start_time, find_cnt)

    start_time = time.time()
    find_cnt = 0
    for n in range(0, test_cnt):
        tar_val = test_cnt
        if list_find_idx(lst, tar_val) >= 0:
            find_cnt += 1
    print("list_find_idx cost: ", time.time() - start_time, find_cnt)


    start_time = time.time()
    find_cnt = 0
    for n in range(0, test_cnt):
        tar_val = random.randint(-data_len, data_len) #test_cnt
        if tar_val in lst:
            find_cnt += 1
    print("in cost: ", time.time() - start_time, find_cnt)

    start_time = time.time()
    find_cnt = 0
    for n in range(0, test_cnt):
        tar_val = test_cnt
        if tar_val in lst:
            find_cnt += 1
    print("in cost: ", time.time() - start_time, find_cnt)

    start_time = time.time()
    find_cnt = 0
    for n in range(0, test_cnt):
        tar_val = random.randint(-data_len, data_len) #test_cnt
        find_cnt += lst.count(tar_val)
    print("count cost: ", time.time() - start_time, find_cnt)