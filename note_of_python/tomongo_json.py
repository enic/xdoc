# -*- utf8-t -*-

import sys
from tracemalloc import start

src_file_name = "60068_battle_data10000_battledata_personal_1579173575501679_20220126181938.json"
src_file = open(src_file_name, "r")
tar_file = open("out.json", "w")
lines = src_file.readlines()
for line in lines:
    #print(line)
    new_line = None
    start_pos = line.find('"battle_id": ')
    if start_pos >= 0:
        start_pos += 13
    else:
        if line.find('"lobby_team_id": 0,') >= 0:
            start_pos = -1
        else:
            start_pos = line.find('"lobby_team_id": ')
            if start_pos >= 0:
                #print(line, line.find(']"'))
                if line.find(']"') >= 0:
                    start_pos += 17
                else:
                    start_pos = -1
                    print(line)
            else:
                start_pos = -1
    if start_pos >= 0:  # "lobby_team_id":没有NumberLong数据会应为少"被吃掉
        end_pos = line.find(" ", start_pos)
        new_line = line[0:start_pos] + "NumberLong(" + line[start_pos+1:end_pos] + "),\n"
        #print(new_line)
    # if line.find('"battle_id": "') >= 0:
    #     print(line)
    # elif line.find('"lobby_team_id": "') >= 0:
    #     print(line)
    tar_file.writelines([line if not new_line else new_line])

