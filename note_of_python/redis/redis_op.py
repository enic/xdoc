# -*- utf8-t -*-

import redis

redis_db = redis.StrictRedis("10.212.22.81", 6379, 0)
#ret = redis_db.set("python_test", 1)
#member = "member1"
#score = 1
#mapping = {member: score,member: score,}
#ret = redis_db.zadd("python_test1", mapping)
#ret = redis_db.zadd("python_test1", {"member1_1": 11, "member1_2": 12, "member1_3": 13, "member1_4": 14, "member1_5": 15})
#print(ret)

# lua
zset_key = "a_ranklist:990099:t:6"
lua_load_rank_list = '\
    local zset_key = KEYS[1]\
    local rank_capacity = tonumber(ARGV[1])\
    local desc_order = tonumber(ARGV[2])\
    local cur_size = redis.call("ZCARD", zset_key)\
    local rm_cnt = 0\
    if cur_size > rank_capacity then\
        if 1 == desc_order then\
            rm_cnt = redis.call("ZREMRANGEBYRANK", zset_key, 0, cur_size - rank_capacity - 1)\
        else\
            rm_cnt = redis.call("ZREMRANGEBYRANK", zset_key, rank_capacity - 1, cur_size)\
        end\
        cur_size = rank_capacity\
    end\
    local last_member = nil\
    local last_score = nil\
    local last_item = nil\
    if cur_size > 0 then\
        if 1 == desc_order then\
            last_item = redis.call("ZRANGE", zset_key, 0, 0, "WITHSCORES")\
        else\
            last_item = redis.call("ZREVRANGE", zset_key, 0, 0, "WITHSCORES")\
        end\
    end\
    if last_item then\
        last_member = tonumber(last_item[1])\
        last_score = tonumber(last_item[2])\
    end\
    return {cur_size, last_member, last_score}\
'
#ret = redis_db.eval(lua_load_rank_list, 1, zset_key, 29, 1)
#print(ret)

# return  add_cnt, last_item, cur_size
_LUA_SCRIPT_UPDATE_RANK_LIST = '\
    local zset_key = KEYS[1]\
    local rank_capacity = tonumber(ARGV[1])\
    local desc_order = tonumber(ARGV[2])\
    local item_cnt = tonumber(ARGV[3])\
    local items_params = {}\
    for idx = 0, item_cnt - 1, 1 do\
        items_params[1 + idx*2] = ARGV[4+idx*2+1]\
        items_params[1 + idx*2 + 1] = ARGV[4+idx*2]\
    end\
    local add_cnt = redis.call("ZADD", zset_key, unpack(items_params))\
    local cur_size = redis.call("ZCARD", zset_key)\
    local rm_cnt = 0\
    if cur_size > rank_capacity then\
        if 1 == desc_order then\
            rm_cnt = redis.call("ZREMRANGEBYRANK", zset_key, 0, cur_size - rank_capacity - 1)\
        else\
            rm_cnt = redis.call("ZREMRANGEBYRANK", zset_key, rank_capacity - 1, cur_size)\
        end\
        cur_size = rank_capacity\
    end\
    local last_member = nil\
    local last_score = nil\
    local last_item = nil\
    if cur_size > 0 then\
        if 1 == desc_order then\
            last_item = redis.call("ZRANGE", zset_key, 0, 0, "WITHSCORES")\
        else\
            last_item = redis.call("ZREVRANGE", zset_key, 0, 0, "WITHSCORES")\
        end\
    end\
    if last_item then\
        last_member = tonumber(last_item[1])\
        last_score = tonumber(last_item[2])\
    end\
    return {cur_size, last_member, last_score}\
'
#return {items_params, item_cnt, ARGV[3+0*2+1], ARGV[3+0*2]}\
#                                                    capacity, desc_order, item_cnt, member1,score1,member2,score2
#ret = redis_db.eval(_LUA_SCRIPT_UPDATE_RANK_LIST, 1, zset_key, 30, 1, 2, 66, 7, 88, 9)
#ret = redis_db.eval(lua_update_rank_list, 1, zset_key, 29, 1,      3,  18, 8, 19, 9, 17, 7)
#print(ret)

# list
# 一个或多个值加入右端
# redis_db.rpush()
# redis_db.lpush()
# rpop
# lpop
# lindex
# lrange
# ltrim
# blpop
# brpop
# rpoplpush
# brpoplpush
# rpushx
# lpushx
# llen
# linsert before

# # zset
# redis_db.zadd("zset:xxx", {"member": 0})
redis_db.zadd("zset:xxx", {"member_1": 1, "member_2": 2})
ret = redis_db.zrevrange("zset:xxx", 0, 999, True)
print(ret)
# redis_db.zrem("zset:xxx", "member1")
# redis_db.zcard("zset:xxx")
# score_diff = 0.1
# redis_db.zincrby("zset:xxx", "member", score_diff)
# redis_db.zcount
# redis_db.zrank
# redis_db.zscore
# redis_db.zrange
# redis_db.zrevrange
# redis_db.zrangebyscore
# redis_db.zrevrangebyscore
# redis_db.zremrangebyrank
# redis_db.zremrangebyscore
# redis_db.zremrangebyrank
# redis_db.zinterstore
# redis_db.zunionstore


# zremrangebyrank

# # htable
# redis_db.hmget
# redis_db.hmset
# redis_db.hdel
# redis_db.hlen
# redis_db.hexists
# redis_db.hkeys
# redis_db.hvals
# redis_db.hgetall
# redis_db.hincrby
# redis_db.hincrbyfloat


# 模拟排行榜
#redis_db.