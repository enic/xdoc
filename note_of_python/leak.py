# -*- coding: utf8 -*-

import sys, gc

class Node():
    def __init__(self):
        self.parent = None
        self.last = None
        self.next = None

    def __del__(self):
        print("Node.__del__")

    def set_parent(self, parent):
        self.parent = parent

class Entity():
    def __init__(self):
        self.prop_map = {}

    def __del__(self):
        print("Entity.__del__")

    def init_props(self):
        for n in range(0, 1):
            node = Node()
            self.prop_map[n] = node
            node.set_parent(self.prop_map)

def test1():
    gc.set_threshold(1,1,1)
    print("----------begin")
    for idx in range(0, 3):
        head = Node()
        cur_node = Node()
        head.next = cur_node
        cur_node.last = head
        #gc.collect(0)
        #gc.collect(1)
        #gc.collect(2)  # 循环引用只有到第三代才会释放
    print("----------end")

def test2():
    #gc.set_threshold(2,1,1)
    #gc.set_debug(gc.DEBUG_STATS | gc.DEBUG_LEAK)
    gc.set_debug(gc.DEBUG_STATS)
    print("----------begin")
    # for idx in range(0, 2):
    #     entity = Entity()
    #     entity.init_props()
    #     # gc.collect(0)
    #     # gc.collect(1)
    #     # gc.collect(2)
    #     gc.collect()
    entity = Entity()
    entity.init_props()
    del entity  # entity销毁以后 里边的prop_map和node都不可达了
    gc.collect(0)
    #print(gc.collect())
    print("----------end")

def TestAuto():
    print("---: begin")
    gc.set_debug(gc.DEBUG_STATS)
    node_a = Node()
    node_b = Node()
    node_a.last = node_b
    node_b.next = node_a
    #a = node_a
    #b = node_b
    print("ref_cnt", sys.getrefcount(node_a), sys.getrefcount(node_b))
    #del node_a
    #del node_b
    #print("ref_cnt", sys.getrefcount(a), sys.getrefcount(b))
    #del node_b  # 手工吧两个都删除了就不可了，保留任意一个其他都还能访问
    #gc.collect(0)
    #gc.collect(1)
    #gc.collect(2)
    tmp = Node()
    gc.collect(0)
    gc.collect(1)
    gc.collect(2)
    gc.collect()
    print("---: end")

if __name__ == '__main__':
    #TestAuto()
    gc.collect(2)
    print("---: exit")
    print(gc.is_tracked(1))
    print(gc.is_tracked(gc))
