# -*- utf8-t -*-
import logging
import pandas    # 强行当xls api不怎么合理api并不是围绕xls设计的，后面有空还是看看openpyxl

LOG_FORMAT = '[%(asctime)s.%(msecs)03d] %(message)s'
logging.basicConfig(format=LOG_FORMAT, datefmt="%Y-%m-%d %H:%M:%S", level=logging.DEBUG)
LOG = logging.getLogger("")


def test_xls_methods():
    '''
    @brief: 熟悉xls相关接口使用
    '''
    xls_file_name = "test.xls"
    # 打开xls: 参数sheet_name=None可以返回全部sheet列表(map<sname,sheet>)，否则返回第一个sheet
    xls_sheet_map = pandas.read_excel(xls_file_name, sheet_name=None)
    LOG.info("open xls: %s" % (xls_file_name))
    sheet_names = []
    for name, sheet in xls_sheet_map.items():
        sheet_names.append(name)
    LOG.info("sheet names: %s" % (sheet_names))
    LOG.info("test first sheet, name=%s" % (sheet_names[0]))
    sheet = xls_sheet_map[sheet_names[0]]  # pandas.core.frame.DataFrame
    # 遍历行: 第一行表头遍历不到
    for row_idx, row in sheet.iterrows():
        #print(type(row)) # pandas.core.series.Series  row其实是不包含title的
        #LOG.info("row_idx=%s,row=%s" % (row_idx, row))
        # 遍历行中的每个字段
        for tmp in row:
            LOG.info(tmp)
    # 遍历列
    # sheet.iteritems()

if __name__ == "__main__":
    LOG.info("start---")
    test_xls_methods()




