# -*- utf8-t -*-

import os, time, sys
import psutil

# 获取cpu使用率
print(psutil.cpu_percent())

# 获取内存使用
print(psutil.virtual_memory())
print(psutil.swap_memory())

# 磁盘
print(psutil.disk_usage("/"))
#print(psutil.disk_usage("d:/"))
#print(psutil.disk_partitions())
print(psutil.disk_io_counters())

# 网络io
print(psutil.net_io_counters())
#print(psutil.net_connections())
#print(psutil.net_if_addrs())
#print(psutil.net_if_stats())

