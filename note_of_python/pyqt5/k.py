# -*- coding: utf-8 -*-

import sys, datetime
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import \
    QApplication, QMessageBox, QWidget, QToolTip, QPushButton, QHBoxLayout
import pyqtgraph as pg
import pyqtgraph.examples
import tushare as ts

class DrawChart():
  def __init__(self, code='300002', start=str(datetime.date.today() - datetime.timedelta(days=200)), end=str(datetime.date.today() + datetime.timedelta(days=1)), ktype='D'):
    self.code = code
    self.start = start
    self.end = end
    self.ktype = ktype
    self.data_list, self.t = self.getData()
 
  def pyqtgraphDrawChart(self):
    try:
      self.item = CandlestickItem(self.data_list)
      self.xdict = {0: str(self.hist_data.index[0]).replace('-', '/'), int((self.t + 1) / 2) - 1: str(self.hist_data.index[int((self.t + 1) / 2)]).replace('-', '/'), self.t - 1: str(self.hist_data.index[-1]).replace('-', '/')}
      self.stringaxis = pg.AxisItem(orientation='bottom')
      self.stringaxis.setTicks([self.xdict.items()])
      self.plt = pg.PlotWidget(axisItems={'bottom': self.stringaxis}, enableMenu=False)
 
      self.plt.addItem(self.item)
      # self.plt.showGrid(x=True, y=True)
 
      return self.plt
    except:
      return pg.PlotWidget()
 
  def getData(self):
    self.start = str(datetime.date.today() - datetime.timedelta(days=150))
    self.end = str(datetime.date.today() + datetime.timedelta(days=1))
    self.hist_data = ts.get_hist_data(self.code, self.start, self.end, self.ktype).sort_index()[-300:-1]
    data_list = []
    t = 0
    for dates, row in self.hist_data.iterrows():
      open, high, close, low, volume, price_change, p_change, ma5, ma10, ma20 = row[:10]
      datas = (t, open, close, low, high, volume, price_change, p_change, ma5, ma10, ma20)
      data_list.append(datas)
      t += 1
    return data_list, t


class CandlestickItem(pg.GraphicsObject):
  def __init__(self, data):
    pg.GraphicsObject.__init__(self)
    self.data = data
    self.generatePicture()
 
  def generatePicture(self):
    self.picture = QtGui.QPicture()
    p = QtGui.QPainter(self.picture)
    p.setPen(pg.mkPen('w'))
    w = (self.data[1][0] - self.data[0][0]) / 3.
    prema5 = 0
    prema10 = 0
    prema20 = 0
    for (t, open, close, min, max, volume, price_change, p_change, ma5, ma10, ma20) in self.data:
      if open > close:
        p.setPen(pg.mkPen('g'))
        p.setBrush(pg.mkBrush('g'))
      else:
        p.setPen(pg.mkPen('r'))
        p.setBrush(pg.mkBrush('r'))
      p.drawLine(QtCore.QPointF(t, min), QtCore.QPointF(t, max))
      p.drawRect(QtCore.QRectF(t - w, open, w * 2, close - open))
      if prema5 != 0:
        p.setPen(pg.mkPen('w'))
        p.setBrush(pg.mkBrush('w'))
        p.drawLine(QtCore.QPointF(t-1, prema5), QtCore.QPointF(t, ma5))
      prema5 = ma5
      if prema10 != 0:
        p.setPen(pg.mkPen('c'))
        p.setBrush(pg.mkBrush('c'))
        p.drawLine(QtCore.QPointF(t-1, prema10), QtCore.QPointF(t, ma10))
      prema10 = ma10
      if prema20 != 0:
        p.setPen(pg.mkPen('m'))
        p.setBrush(pg.mkBrush('m'))
        p.drawLine(QtCore.QPointF(t-1, prema20), QtCore.QPointF(t, ma20))
      prema20 = ma20
    p.end()
 
  def paint(self, p, *args):
    p.drawPicture(0, 0, self.picture)
 
  def boundingRect(self):
    return QtCore.QRectF(self.picture.boundingRect())

class KChartWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.init_ui()
    
    def init_ui(self):
        hbox = QHBoxLayout(self)
        drawChart = DrawChart(ktype='D')
        hbox.addWidget(drawChart.pyqtgraphDrawChart())

if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = KChartWidget()
    w.resize(1024, 800)
    w.show()
    sys.exit(app.exec())
