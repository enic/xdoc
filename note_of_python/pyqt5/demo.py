# -*- coding: utf-8 -*-
# http://code.py40.com/pyqt5/16.html

import sys, datetime
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import \
    QApplication, QMessageBox, QWidget, QToolTip, QPushButton
import pyqtgraph
import tushare

class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.close_btn = None
        self.msg_box_btn = None
        self.init_ui()
    
    def init_ui(self):
        QToolTip.setFont(QtGui.QFont("SansSerif", 10))
        self.setToolTip("this is a <b>QWidget</b> widget")
        self.close_btn = QPushButton("close", self)
        self.close_btn.setToolTip("this is a <b>QPushButton</b> widget")
        self.close_btn.resize(self.close_btn.sizeHint())
        self.close_btn.move(50, 50)
        self.setGeometry(300, 300, 300, 300)
        self.setWindowTitle("Tooltips")
        self.msg_box_btn = QPushButton("msg_box")
        self.msg_box_btn.resize(self.close_btn.sizeHint())
        self.close_btn.move(100, 50)
        self._bind_ui_event()

    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        #return super().closeEvent(a0)
        event.ignore()

    def show_msg_box(self):
        reply = QMessageBox.question(self, "message",
            "quit or not", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QApplication.Yes:
            QtCore.QCoreApplication.instance().quit()

    def _bind_ui_event(self):
        self.close_btn.clicked.connect(QtCore.QCoreApplication.instance().quit)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = Example()
    ex.show()
    sys.exit(app.exec())
